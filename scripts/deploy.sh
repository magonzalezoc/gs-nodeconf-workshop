#!/bin/sh
VERSION=$(echo $CI_COMMIT_REF_NAME | awk -F"_" '{print $1}')
echo ${GCLOUD_SERVICE_KEY} > ${HOME}/gcloud-service-key.json
gcloud auth activate-service-account --key-file ${HOME}/gcloud-service-key.json
gcloud config set project ${GCLOUD_PROJECT_ID}
gcloud container clusters get-credentials ${GCLOUD_CLUSTER_NAME} --zone us-central1-a --project ${GCLOUD_PROJECT_ID}
gcloud components install kubectl
sed -i "s|FULL_REPOSITORY_NAME|us.gcr.io/${GCLOUD_PROJECT_ID}/${APP_NAME}:${VERSION}|" "./services/$APP_NAME/$APP_NAME-deployment.yaml"
kubectl apply -f "services/$APP_NAME/$APP_NAME-deployment.yaml"
if [ "$APP_NAME" != "back-end" ]; then
    kubectl apply -f "services/$APP_NAME/$APP_NAME-gateway.yaml"
fi